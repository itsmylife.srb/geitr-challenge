# getir-challenge

The API server runs on the default PORT 3000

# Code Structure

The entire codebase has 3 main parts i.e. controllers, models, services

controller is responsible for busness logic
models contains the Collection
service is responsible to do the DB level operation

# Framework

Express
# Database

MongoDB - main database to save the record

# Prerequisite
Docker is needed to the start entire project. Please make sure docker is running in the system and 3000 port is free to use.

# Project building

`docker build -t getir-task .`
`docker run -e MONGO_URI=<MONGO_URL_PATH> -p 3000:3000 getir-task:latest`

# Swagger Documentation

Open `http://localhost:3000` to read the swagger documentation

