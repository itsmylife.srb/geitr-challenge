const dataService = require('../services/data.service')
const HTTPResponse = require('../utils/http-response')
const httpStatus = require('../utils/http-status-code')

const getData = async (req, res) => {
    try {
        const payload = req.body;
        const response = await dataService.fetchData(payload)
        res.locals.data = response
        return new HTTPResponse(httpStatus.OK)
            .send(req, res);
    } catch (err) {
        return new HTTPResponse(httpStatus.BAD_REQUEST)
            .send(req, res);
    }
}

module.exports = {
    getData
}
