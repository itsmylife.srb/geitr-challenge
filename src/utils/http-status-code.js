(function () {
    "use strict";
  
    var Response = {};
  
    Response.OK = {
      http_status_code: 200,
      status: "SUCCESS",
      message: "Success",
      code: 0
    };
  
    Response.CREATED = {
      http_status_code: 201,
      status: "SUCCESS",
      message: "The request has been fulfilled and resulted in a new resource being created.",
      code: 1
    };
  
    Response.ACCEPTED = {
      http_status_code: 202,
      status: "SUCCESS",
      message: "The request has been accepted for processing, but the processing has not been completed.",
      code: 2

    };
  
    Response.BAD_REQUEST = {
      http_status_code: 400,
      status: "ERROR",
      message: "The request could not be understood by the server due to malformed syntax.",
      code: -1
    };

  
    Response.FORBIDDEN = {
      http_status_code: 403,
      status: "ERROR",
      message: "The server understood the request, but is refusing to fulfill it." +
      " Authorization will not help and the request SHOULD NOT be repeated.",
      code: -3
    };
  
    Response.NOT_FOUND = {
      http_status_code: 404,
      status: "ERROR",
      message: "The requested resource could not be found but may be available again in the future.",
      code: -4
    };
  
    Response.INTERNAL_SERVER_ERROR = {
      http_status_code: 500,
      status: "ERROR",
      message: "The server encountered an unexpected condition which prevented it from" +
      " fulfilling the request."
    };
  
    Response.NOT_IMPLEMENTED = {
      http_status_code: 501,
      status: "ERROR",
      message: "The server either does not recognise the request method, or it lacks" +
      " the ability to fulfill the request."
    };
  
    Response = Object.freeze(Response);
  
    module.exports = Response;
  
  })();
  