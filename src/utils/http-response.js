

const _ = require("lodash"),
    changeCaseObject = require("change-case-object");

(function () {
    "use strict";

    function initOut() {
        return {
            code: -1, // HTTP Status Code
            msg: "", // Status of the response: success / error
            records: null // final data to be sent as output
        };
    }
    /**
     * modifies the response in the desired format
     * code: number
     * msg: string
     * records: object
     */
    var out = initOut();

    /**
     * @description Create a new Response object
     * @param {object} respConstant - Response respConstant which is use as a key for response constants
     * @param {object} [errorConstant] - Error constant to be merged with respConstant
     * @param {object} [msgVars] - Dynamic values can be inserted inside a message by passing
     * it as a key-value pair in the msgVars object
     * @class
     */
    function Response(respConstant, errorConstant, msgVars) {

        out = initOut();

        // if respConstant param is not present..
        if (!respConstant) {

            // ..throw an exception saying param not found
            throw new ReferenceError("respConstant not found");
        }

        respConstant = JSON.parse(JSON.stringify(respConstant));

        // set the out object params with the response values defined by respConstant from response constants
        out.http_status_code = respConstant.http_status_code;

        // out.app_status_code = errorConstant ? errorConstant.app_status_code : -1;

        out.code = respConstant.code;

        if (errorConstant) {
            errorConstant = JSON.parse(JSON.stringify(errorConstant));

            out.msg = errorConstant.msg || errorConstant;

            out.msg = out.msg ? out.msg : '';

            // log msg is not to be sent to the client but to the log server, hence deleted
            delete out.msg.log;

            if (errorConstant.msgVars) {
                msgVars = _.merge(msgVars, errorConstant.msgVars);
            }
        } else {
            out.msg = respConstant.message
        }
    }

    /**
     * @method send method to send an HTTP response for the request
     * @param {object} req - ExpressJS req object
     * @param {object} res - ExpressJS res object
     * @param {object} res.locals.totalData - total data present in the server for the given query
     * @param {object} res.locals.data - the actual data
     */
    Response.prototype.send = function (req, res) {

        // if the Express req object is not present, throw exception
        if (!req) {
            throw new ReferenceError("req param not found");
        }

        // if the Express res object is not present, throw exception
        if (!res) {
            throw new ReferenceError("res param not found");
        }

        // if req object does not have both query and body, throw exception
        if (!req.query && !req.body) {
            throw new ReferenceError("either req.query or req.body should be present");
        }

        // if data is present, add data in records
        if (res.locals.data) {
            out.records = res.locals.data;
        }

        out = changeCaseObject.camelCase(JSON.parse(JSON.stringify(out)));
        let httpStatusCode = out.httpStatusCode
        delete out.httpStatusCode

        // request the ExpressJS response object to send the response
        res
            // define CORS control
            .set("Access-Control-Allow-Origin", "*")
            .set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
            .set("Access-Control-Allow-Headers", "Authorization, Content-Type")
            .set("Access-Control-Allow-Credentials", false)
            .set("access-control-expose-headers", "Authorization")

            // sets the HTTP Status Code
            .status(httpStatusCode)

            // sets the response Content-Type to application/json and sends the json data
            .send(out);

    };

    // return the Response class
    module.exports = Response;
})();
