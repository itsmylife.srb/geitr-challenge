const express = require('express');
const validate = require('../../middlewares/validate');
const { getData } = require('../../controllers/data.controller');
const { FecthDataDto } = require('../../dto/data.dto');
const router = express.Router();

router.route('/')
    .post(validate(FecthDataDto), getData)

module.exports = router