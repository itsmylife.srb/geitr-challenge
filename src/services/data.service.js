const logger = require("../config/logger")
const data = require("../models/data.model")
const moment = require('moment')

const fetchData = (payload) => {
    return new Promise(async (resolve, reject) => {
        try {
            const { startDate, endDate, minCount, maxCount } = payload
            const pipeline = [
                {
                    '$unwind': {
                        'path': '$counts',
                        'preserveNullAndEmptyArrays': true
                    }
                }, {
                    '$group': {
                        '_id': '$_id',
                        'key': {
                            '$first': '$key'
                        },
                        'createdAt': {
                            '$first': '$createdAt'
                        },
                        'totalCount': {
                            '$sum': '$counts'
                        }
                    }
                }, {
                    '$match': {
                        'createdAt': {
                            '$gte': new Date(startDate),
                            '$lt': new Date(moment(endDate).add(1, 'd').format('YYYY-MM-DD')) //handling the same date scenario, it will return all value from startOfDay to endOfDay
                        },
                        'totalCount': {
                            '$gte': minCount,
                            '$lte': maxCount
                        }
                    }
                }, {
                    '$project': {
                        'createdAt': 1,
                        '_id': 0,
                        'key': 1,
                        'totalCount': 1
                    }
                }
            ]
            const results = await data.aggregate(pipeline).allowDiskUse(true)
            return resolve(results)
        } catch (err) {
            logger.error(err)
            return reject(err)
        }
    })
}

module.exports = {
    fetchData
}
