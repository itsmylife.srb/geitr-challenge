const Joi = require('joi');
const moment = require('moment')
const dateFormat = (value, helpers) => {
    if (!moment(value, 'YYYY-MM-DD', true).isValid()) {
        return helpers.message('Date format should be YYYY-MM-DD');
    }
    return value;
};
const FecthDataDto = {
    body: Joi.object().keys({
        startDate: Joi.string().required().custom(dateFormat),
        endDate: Joi.string().required().custom(dateFormat),
        minCount: Joi.number().required(),
        maxCount: Joi.number().required(),
    }),
};



module.exports = {
    FecthDataDto
}
