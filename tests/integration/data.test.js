const faker = require('faker');
const request = require('supertest');
const app = require('../../src/app');
const Response = require('../../src/utils/http-status-code');
const mongoose = require('mongoose');
const config = require('../../src/config/config');
const moment = require('moment')
const { matchers } = require('jest-date/matchers')

jest.setTimeout(15 * 1000)
expect.extend({ toBeBefore: matchers.toBeBefore, toBeAfter: matchers.toBeAfter })


describe('fetch data', () => {
  beforeAll(async () => {
    await mongoose.connect(config.mongoose.url, config.mongoose.options);
    console.log('testdb connected')
  });

  describe('DTO validation', () => {
    let startDate, endDate, maxCount, minCount;
    beforeEach(() => {
      startDate = moment(faker.datatype.datetime()).format('YYYY-MM-DD')
      endDate = moment(faker.datatype.datetime()).format('YYYY-MM-DD')
      maxCount = faker.datatype.number()
      minCount = faker.datatype.number()
    })

    test(`should return 400 with invalid date format`, async () => {
      let payload = {
        startDate: "2021-02-29",
        endDate,
        maxCount,
        minCount
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.BAD_REQUEST.http_status_code)
      expect(res.body).toEqual({
        code: Response.BAD_REQUEST.http_status_code,
        msg: "Date format should be YYYY-MM-DD"
      })
    })
    test(`should return 400 with  minCount as required`, async () => {
      let payload = {
        startDate,
        endDate,
        maxCount
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.BAD_REQUEST.http_status_code)
      expect(res.body).toEqual({
        code: Response.BAD_REQUEST.http_status_code,
        msg: "\"minCount\" is required"
      })
    })

    test(`should return 400 with  maxCount as required`, async () => {
      let payload = {
        startDate,
        endDate,
        minCount
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.BAD_REQUEST.http_status_code)
      expect(res.body).toEqual({
        code: Response.BAD_REQUEST.http_status_code,
        msg: "\"maxCount\" is required"
      })
    })

    test(`should return 400 with  endDate as required`, async () => {
      let payload = {
        startDate,
        minCount,
        maxCount
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.BAD_REQUEST.http_status_code)
      expect(res.body).toEqual({
        code: Response.BAD_REQUEST.http_status_code,
        msg: "\"endDate\" is required"
      })
    })

    test(`should return 400 with  startDate as required`, async () => {
      let payload = {
        endDate,
        minCount,
        maxCount
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.BAD_REQUEST.http_status_code)
      expect(res.body).toEqual({
        code: Response.BAD_REQUEST.http_status_code,
        msg: "\"startDate\" is required"
      })
    })
  })
  describe('POST /v1/data', () => {
    let startDate, endDate, maxCount, minCount;
    beforeEach(() => {
      startDate = faker.date.between('01-01-2014', '01-01-2018')
      endDate = faker.date.past(startDate.getFullYear(), startDate)
      maxCount = faker.datatype.number(1000)
      minCount = faker.datatype.number(maxCount)
    })
    test(`should return 200 and code 0 with message 'success' with real data`, async () => {
      let payload = {
        startDate: '2015-05-03',
        endDate: '2018-01-01',
        minCount: 5,
        maxCount: 200
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.OK.http_status_code)
      expect(res.body).toEqual({
        code: Response.OK.code,
        msg: Response.OK.message,
        records: expect.any(Array)
      })
      res.body.records.forEach(record => {
        expect(new Date(payload.startDate)).toBeBefore(new Date(record.createdAt))
        expect(record.totalCount).toBeGreaterThanOrEqual(payload.minCount)
        expect(record.totalCount).toBeLessThanOrEqual(payload.maxCount)
      })
    })
    test(`should return 200 and code 0 with message 'success' with random data`, async () => {
      let payload = {
        startDate: moment(startDate).format('YYYY-MM-DD'),
        endDate: moment(endDate).format('YYYY-MM-DD'),
        minCount,
        maxCount
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.OK.http_status_code)
      expect(res.body).toEqual({
        code: Response.OK.code,
        msg: Response.OK.message,
        records: expect.any(Array)
      })
      res.body.records.forEach(record => {
        expect(new Date(payload.startDate)).toBeBefore(new Date(record.createdAt))
        expect(record.totalCount).toBeGreaterThanOrEqual(payload.minCount)
        expect(record.totalCount).toBeLessThanOrEqual(payload.maxCount)
      })
    })

    test(`startDate and endDate is same`, async () => {
      let payload = {
        startDate: '2016-11-11',
        endDate: '2016-11-11',
        minCount: 10,
        maxCount: 100
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.OK.http_status_code)
      expect(res.body).toEqual({
        code: Response.OK.code,
        msg: Response.OK.message,
        records: expect.any(Array)
      })
      res.body.records.forEach(record => {
        expect(moment(record.createdAt).format('YYYY-MM-DD')).toEqual(payload.startDate)
        expect(record.totalCount).toBeGreaterThanOrEqual(payload.minCount)
        expect(record.totalCount).toBeLessThanOrEqual(payload.maxCount)
      })
    })

    test(`startDate is ahead of endDate`, async () => {
      let payload = {
        startDate: '2016-12-11',
        endDate: '2016-11-11',
        minCount: 10,
        maxCount: 100
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.OK.http_status_code)
      expect(res.body).toEqual({
        code: Response.OK.code,
        msg: Response.OK.message,
        records: []
      })
      res.body.records.forEach(record => {
        expect(moment(record.createdAt).format('YYYY-MM-DD')).toEqual(payload.startDate)
        expect(record.totalCount).toBeGreaterThanOrEqual(payload.minCount)
        expect(record.totalCount).toBeLessThanOrEqual(payload.maxCount)
      })
    })

    test(`mincount is more than maxcount`, async () => {
      let payload = {
        startDate: '2015-12-11',
        endDate: '2016-11-11',
        minCount: 100,
        maxCount: 10
      }
      const res = await request(app)
        .post('/v1/data')
        .send(payload)
        .expect(Response.OK.http_status_code)
      expect(res.body).toEqual({
        code: Response.OK.code,
        msg: Response.OK.message,
        records: []
      })
      res.body.records.forEach(record => {
        expect(moment(record.createdAt).format('YYYY-MM-DD')).toEqual(payload.startDate)
        expect(record.totalCount).toBeGreaterThanOrEqual(payload.minCount)
        expect(record.totalCount).toBeLessThanOrEqual(payload.maxCount)
      })
    })



  })

  afterAll(async () => {
    await mongoose.disconnect();
  });
})
